﻿using UnityEngine;
using System.Collections;

public enum Needs
{
    NONE,
    ALL,
    HUNGRY,
    SLEEPY
}

public enum Moods
{
    NONE,
    ALL,
    HAPPY,
    RAGE,
}

public enum HealthCondition
{
    NONE,
    ALL,
    HEALTHY,
    RAGE,
    DIRTY
}

public enum StayPet
{
    NONE,
    ALL,
    AWAKE,
    SLEEPING
}

public class BeingBrain
{
    GameObject petBudy;

    public BeingBrain(GameObject petBudy)
    {
        this.petBudy = petBudy;
    }

    public void Feeling()
    {


    }

    public void Eat()
    {
        Debug.Log("needEat");
    }

    void Memory()
    {


    }
}
