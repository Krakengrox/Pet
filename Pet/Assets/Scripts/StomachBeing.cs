﻿using UnityEngine;
using System.Collections;

public class StomachBeing
{

    int hungryLevel;
    int stomachFull;
    BeingBrain beingBrain = null;

    public StomachBeing(BeingBrain beingBrain)
    {
        this.beingBrain = beingBrain;
        Init();
    }

    public void Init()
    {

    }

    public void CheckHungry()
    {
        if (stomachFull < hungryLevel) beingBrain.Eat();
    }
}
