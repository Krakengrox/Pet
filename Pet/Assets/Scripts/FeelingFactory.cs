﻿using UnityEngine;
using System.Collections;

public class FeelingFactory
{

    Needs WhatNeeds(Needs needs, int lvl)
    {
        switch (needs)
        {
            case Needs.NONE:
                break;
            case Needs.ALL:
                break;
            case Needs.HUNGRY:
                break;
            case Needs.SLEEPY:
                break;
            default:
                break;
        }
        return Needs.NONE;
    }

    Moods WhatFeels(Moods moods)
    {
        switch (moods)
        {
            case Moods.NONE:
                break;
            case Moods.ALL:
                break;
            case Moods.HAPPY:
                break;
            case Moods.RAGE:
                break;
            default:
                break;
        }

        return Moods.NONE;
    }

    HealthCondition WhatCondition(HealthCondition healthCondition)
    {
        switch (healthCondition)
        {
            case HealthCondition.NONE:
                break;
            case HealthCondition.ALL:
                break;
            case HealthCondition.HEALTHY:
                break;
            case HealthCondition.RAGE:
                break;
            case HealthCondition.DIRTY:
                break;
            default:
                break;
        }
        return HealthCondition.NONE;
    }
}
