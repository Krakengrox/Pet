﻿using UnityEngine;
using System.Collections;

public class Being : Singleton<Being>
{
    StomachBeing stomachBeing = null;
    BeingBrain beingBrain = null;
    GameObject petBody;

    protected override void Awake()
    {
        base.Awake();
    }

    void Init()
    {
        ShowBeing();
    }

    void WakeUpBeing()
    {
        this.beingBrain = new BeingBrain(ShowBeing());
        this.stomachBeing = new StomachBeing(this.beingBrain);
    }

    GameObject ShowBeing()
    {
        this.petBody = new GameObject();
        return petBody;
    }


}
